FROM python:3.9

# install linux packages
RUN apt-get update -y
RUN apt-get install apt-utils -y
RUN apt-get install nano -y

# set a directory for the app
WORKDIR /usr/src/app

# copy all the files to the container
COPY . $WORKDIR

# install dependencies
RUN pip install --no-cache-dir -r ./requirements.txt

# run the command
# CMD ["pytest . -v --profile"]

