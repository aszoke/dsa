from ..folding_hash import calculate_folding_hash


def test_calculate_folding_hash():

    assert calculate_folding_hash(4365554601, 11) == 1


def test_calculate_folding_hash_sentence():
    text = "Java language"
    chars = []
    chars.extend(text)

    asc_str = ""
    for c in chars:
        asc_str += str(ord(c))

    assert calculate_folding_hash(int(asc_str), 100000) == 660
