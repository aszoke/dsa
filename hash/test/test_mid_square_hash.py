from ..mid_square_hash import calculate_mid_square_hash


def test_calculate_mid_square_hash():

    assert calculate_mid_square_hash(4365554601, 11) == 10


def test_calculate_mid_square_hash_sentence():
    text = "Java language"
    chars = []
    chars.extend(text)

    asc_str = ""
    for c in chars:
        asc_str += str(ord(c))

    assert calculate_mid_square_hash(int(asc_str), 100000) == 45
