"""
Folding hash module
"""


def calculate_folding_hash(number: int, hash_size: int) -> int:
    """
    **FOLDING HASH** method for constructing hash functions begins by dividing the item into equal-size pieces (the last
    piece may not be of equal size). These pieces are then added together to give the resulting hash value.

    A hash table is a collection of items which are stored in such a way as to make it easy to find them later. Each
    position of the hash table, often called a slot, can hold an item and is named by an integer value starting at 0.
    A hash table is a data structure that maps keys to values for highly efficient lookup.

    **For example**: if our item was the phone number 436-555-4601, we would take the digits and divide them into
    groups of 2 (43,65,55,46,01). After the addition, 43+65+55+46+01, we get 210. If we assume our hash table has 11
    slots, then we need to perform the extra step of dividing by 11 and keeping the remainder. In this case 210 % 11
    is 1, so the phone number 436-555-4601 hashes to slot 1. Some folding methods go one step further and reverse
    every other piece before the addition. For the above example, we get 43+56+55+64+01=219 which gives 219 % 11=10.

    :param number:
        hashable number
    :param hash_size:
        number of slots
    :return:
        hash value

    **Complexity**
    Hash tables can provide constant time (𝑂(1)) searching.

    **See also**
    :mod:`hash.mid_square_hash`
    """
    number_list = list(str(number))
    hash_value = 0
    while len(number_list) > 0:
        hash_value = hash_value + int(''.join([str(elem) for elem in number_list[0:2]]))
        number_list = number_list[2:]

    return hash_value % hash_size
