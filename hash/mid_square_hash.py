"""
Mid square hash module
"""


def calculate_mid_square_hash(number: int, hash_size: int) -> int:
    """
    **MID SQUARE HASH** method first squares the item, and then extract some portion of the resulting digits.

    A hash table is a collection of items which are stored in such a way as to make it easy to find them later. Each
    position of the hash table, often called a slot, can hold an item and is named by an integer value starting at 0.
    A hash table is a data structure that maps keys to values for highly efficient lookup.

    **For example**: if the item were 44, we would first compute 442=1,936. By extracting the middle two digits, 93,
    and performing the remainder step, we get 5 (93 % 11).

    A hash table is a data structure that maps keys to values for highly efficient lookup.
    Hash tables can provide constant time (𝑂(1)) searching.

    :param number:
        hashable number
    :param hash_size:
        number of slots
    :return:
        hash value

    **See also**
    :mod:`hash.folding_hash`
    """
    square = pow(number, 2)
    square_list = list(str(square))
    middle = len(square_list) // 2
    mid_square = square_list[middle:middle+2]
    hash_value = int(''.join([str(elem) for elem in mid_square])) % hash_size
    return hash_value
