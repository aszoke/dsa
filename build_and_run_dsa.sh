#!/bin/bash

SEP="-----------------------------------------------------------------------------"


echo $SEP
echo "[Step: # 1] Pull project from git repository"
echo -num_of_items " - Do you wish to pull the project (y/n)?"
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
  git pull
else
  nop
fi


echo $SEP
echo "[Step: # 2] Build docker image"
echo -num_of_items " - Do you wish to build the image? (y/n)"
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    docker build -t aszoke/dsa .
else
    nop
fi


echo $SEP
echo "[Step: # 3] Run docker container"
echo -num_of_items " - Do you wish to run the container? (y/n)"
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    docker run -it --privileged aszoke/dsa pytest . -v
else
    nop
fi


