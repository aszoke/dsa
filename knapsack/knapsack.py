"""
0-1 Knapsack naive solution module

**Problem statement**:
Given weights (item_weight) and values (item_value) of num_of_items items, put these items in a knapsack of capacity
knapsack_capacity to get the maximum total value in the knapsack. In other words, given two integer arrays
item_value[0..num_of_items-1] and item_weight[0..num_of_items-1] which represent values and weights associated with
num_of_items items respectively. Also given an integer knapsack_capacity which represents knapsack capacity, find out
the maximum value subset of item_value[] such that sum of the weights of this subset is smaller than or equal to
knapsack_capacity. You cannot break an item, either pick the complete item or don’t pick it (0-1 property).
"""


def knapsack(knapsack_capacity: int, item_weight: list, item_value: list, num_of_items: int) -> int:
    """
    **0-1 KNAPSACK PROBLEM WITH NAIVE SOLUTION**

    The naive recursive solution is to consider all subsets of items and calculate the total weight and value of all
    subsets. Consider the only subsets whose total weight is smaller than knapsack_capacity. From all such subsets,
    pick the maximum value subset.

    In the following recursion tree, K() refers to knapSack(). The two parameters indicated in the following recursion
    tree are num_of_items and knapsack_capacity. The recursion tree is for following sample inputs:
    item_weight[] = {1, 1, 1}, knapsack_capacity = 2, item_value[] = {10, 20, 30}
                        K(num_of_items, knapsack_capacity)
                          K(3, 2)
                      /            \
                    /                \
               K(2, 2)                  K(2, 1)
             /        \\                  /    \
           /           \\               /       \
        K(1, 2)      K(1, 1)        K(1, 1)     K(1, 0)
         /    \\       /     \\        /     \
        /      \\     /       \\      /       \
    K(0, 2)  K(0, 1)  K(0, 1)  K(0, 0)  K(0, 1)   K(0, 0)
    Recursion tree for Knapsack capacity 2 units and 3 items of 1 unit weight.

    **Note**
    1) that the above function computes the same sub-problems again and again. See the following recursion tree, K(1, 1)
     is being evaluated twice. The time complexity of this naive recursive solution is exponential (2^num_of_items) -
     as it is a binary tree!
    2) The recursion is actually a DFS - more specifically - an inorder traversal

    :param knapsack_capacity: knapsack capacity
    :param item_weight: item weight
    :param item_value: item value
    :param num_of_items: number of items
    :return: maximum value subset of item_values

    **See also**
    :mod:`knapsack.knapsack_with_dynamic_programming`
    """

    # Base Case: all items are packed or the capacity is filled with items
    if num_of_items == 0 or knapsack_capacity == 0:
        return 0

    # To consider all subsets of items, there can be two cases for every item:
    # Case 1: If weight of the nth item (num_of_items.b. its index is num_of_items-1) is more than Knapsack of capacity
    # knapsack_capacity,then this item cannot be included in the optimal solution
    if item_weight[num_of_items - 1] > knapsack_capacity:
        return knapsack(knapsack_capacity, item_weight, item_value, num_of_items - 1)

    # Case 2: If weight of the nth item (num_of_items.b. its index is num_of_items-1) is less than Knapsack of capacity
    # knapsack_capacity, than the item is included in the optimal subset.
    # Therefore, the maximum value that can be obtained from num_of_items items is the max of the following two
    # values.
    # (1) Value of nth item plus maximum value obtained by num_of_items-1 items and knapsack_capacity minus the weight
    # of the nth item (including nth item).
    # (2) Maximum value obtained by num_of_items-1 items and knapsack_capacity weight (excluding nth item)
    else:
        including_case = item_value[num_of_items - 1] + \
                knapsack(knapsack_capacity - item_weight[num_of_items - 1], item_weight, item_value, num_of_items - 1)
        excluding_case = knapsack(knapsack_capacity, item_weight, item_value, num_of_items - 1)
        # selecting the biggest from the two branches considering the value of the nodes (OPTIMIZATION!)
        max_value = max(including_case, excluding_case)
        # print(str(mx), end=' ')
        return max_value
