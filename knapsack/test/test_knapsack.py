import pytest

from knapsack import knapsack as ks
from . import data


@pytest.mark.parametrize("input_params, expected", data.basic_case)
def test_knapsack_basic_case(input_params, expected):
    capacity = input_params[0]
    item_value = input_params[1]
    item_weight = input_params[2]
    num_of_items = len(input_params[2])
    assert ks.knapsack(capacity, item_weight, item_value, num_of_items) == expected
