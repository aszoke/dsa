import pytest

from knapsack import knapsack_with_dynamic_programming as kd
from . import data


@pytest.mark.parametrize("input_params, expected", data.basic_case)
def test_knapsack_with_dynamic_programming_basic_case(input_params, expected):
    capacity = input_params[0]
    item_value = input_params[1]
    item_weight = input_params[2]
    num_of_items = len(input_params[2])
    assert kd.knapsack_with_dinalic_programming(capacity, item_weight, item_value, num_of_items) == expected


@pytest.mark.parametrize("input_params, expected", data.huge_case)
def test_knapsack_with_dynamic_programming_huge_case(input_params, expected):
    capacity = input_params[0]
    item_value = input_params[1]
    item_weight = input_params[2]
    num_of_items = len(input_params[2])
    assert kd.knapsack_with_dinalic_programming(capacity, item_weight, item_value, num_of_items) == expected
