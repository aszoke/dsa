"""
0-1 Knapsack dynamic programming solution module

**Problem statement**:
Given weights (item_weight) and values (item_value) of num_of_items items, put these items in a knapsack of capacity
knapsack_capacity to get the maximum total value in the knapsack. In other words, given two integer arrays
item_value[0..num_of_items-1] and item_weight[0..num_of_items-1] which represent values and weights associated with
num_of_items items respectively. Also given an integer knapsack_capacity which represents knapsack capacity, find out
the maximum value subset of item_value[] such that sum of the weights of this subset is smaller than or equal to
knapsack_capacity. You cannot break an item, either pick the complete item or don’t pick it (0-1 property).
"""

memo = {}  # data structure for memoization


def knapsack_with_dinalic_programming(knapsack_capacity: int, item_weight: list, item_value: list, num_of_items: int) \
        -> int:
    """
    **0-1 KNAPSACK PROBLEM WITH DYNAMIC PROGRAMMING SOLUTION**

    Dynamic programming is breaking down a problem into smaller sub-problems, solving each sub-problem and storing the
    solutions to each of these sub-problems in an array (or similar data structure) so each sub-problem is only
    calculated once.

    **Remark**
    Question: When should I solve this problem with dynamic programming?
    Answer: Intractable problems are those that run in exponential time. They're slow. Intractable problems are those
    that can only be solved by bruteforcing through every single combination (NP hard).

    Memoization: is the act of storing a solution.

    :param knapsack_capacity: knapsack capacity
    :param item_weight: item weight
    :param item_value: item value
    :param num_of_items: number of items
    :return: maximum value subset of item_values

    **See also**
    :mod:`knapsack.knapsack`
    """
    if num_of_items == 0 or knapsack_capacity == 0:
        return 0

    if item_weight[num_of_items - 1] > knapsack_capacity:
        return knapsack_with_dinalic_programming(knapsack_capacity, item_weight, item_value, num_of_items - 1)
    else:
        if memo.get((num_of_items - 1, knapsack_capacity - item_weight[num_of_items - 1])) is None:
            memo[(num_of_items - 1, knapsack_capacity - item_weight[num_of_items - 1])] = \
                item_value[num_of_items - 1] + \
                knapsack_with_dinalic_programming(knapsack_capacity - item_weight[num_of_items - 1],
                                                  item_weight, item_value, num_of_items - 1)

        if memo.get((num_of_items - 1, knapsack_capacity)) is None:
            memo[(num_of_items - 1, knapsack_capacity)] = \
                knapsack_with_dinalic_programming(knapsack_capacity, item_weight, item_value, num_of_items - 1)

        max_value = max(memo.get((num_of_items - 1, knapsack_capacity - item_weight[num_of_items - 1])),
                        memo.get((num_of_items - 1, knapsack_capacity)))
        # print(str(mx), end=' ')
        return max_value
