"""
Binary heap module
"""


class BinHeap:
    """
    **BINARY HEAP** is a binary tree with following properties.

    1) Shape property: a binary heap is a complete binary
    tree; that is, all levels of the tree, except possibly the last one (deepest) are fully filled, and, if the last
    level of the tree is not complete, the nodes of that level are filled from left to right.

    2) Heap property: the key stored in each node is either greater than or equal to (≥) or less than or equal to (≤)
    the keys in the node's children, according to some total order.

    The binary heap has two common variations: the min heap, in which the smallest key is always at the front,
    and the max heap, in which the largest key value is always at the front.

    **Complexity**
    O(log num)
    """
    def __init__(self):
        self.heap_list = [0]
        self.current_size = 0

    def _percolation_up(self, i):
        while i // 2 > 0:
            if self.heap_list[i] < self.heap_list[i // 2]:
                tmp = self.heap_list[i // 2]
                self.heap_list[i // 2] = self.heap_list[i]
                self.heap_list[i] = tmp
            i = i // 2

    def _percolation_down(self, i):
        while (i * 2) <= self.current_size:
            min_child = self._min_child(i)
            if self.heap_list[i] > self.heap_list[min_child]:
                tmp = self.heap_list[i]
                self.heap_list[i] = self.heap_list[min_child]
                self.heap_list[min_child] = tmp
            i = min_child

    def _min_child(self, i):
        if i * 2 + 1 > self.current_size:
            return i * 2
        else:
            if self.heap_list[i * 2] < self.heap_list[i * 2 + 1]:
                return i * 2
            else:
                return i * 2 + 1

    def insert(self, k):
        """
        Adds a new item to the heap
        :param k: item to be added
        """
        self.heap_list.append(k)
        self.current_size = self.current_size + 1
        self._percolation_up(self.current_size)

    def remove_min(self) -> int:
        """
        Returns the item with the minimum key value, removing the item from the heap
        :return: removed item
        """
        retval = self.heap_list[1]
        self.heap_list[1] = self.heap_list[self.current_size]
        self.current_size = self.current_size - 1
        self.heap_list.pop()
        self._percolation_down(1)
        return retval

    def build_heap(self, alist):
        """
        Builds a new heap from a list of keys
        :param alist: list from which the new heap is built
        """
        i = len(alist) // 2
        self.current_size = len(alist)
        self.heap_list = [0] + alist[:]
        while i > 0:
            self._percolation_down(i)
            i = i - 1

    def get_size(self):
        return len(self.heap_list) - 1  # Note: we are indexing from 1
