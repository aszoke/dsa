import pytest
from ..binary_heap import BinHeap


@pytest.fixture
def sample_data():
    return [4, 8, 7, 2, 9, 10, 5, 1, 3, 6]


@pytest.fixture
def sample_data_len(sample_data):
    return len(sample_data)


@pytest.fixture
def sample_data_ordered(sample_data):
    return sorted(sample_data)


@pytest.fixture
def bin_heap(sample_data):
    min_bh = BinHeap()
    for i in sample_data:
        min_bh.insert(i)
    return min_bh


@pytest.fixture
def bin_heap2(sample_data):
    min_bh = BinHeap()
    min_bh.build_heap(sample_data)
    return min_bh


def test_priority_queue_size(bin_heap, sample_data_len):
    assert bin_heap.get_size() == sample_data_len


def test_priority_queue_items(bin_heap2, sample_data_ordered):
    removed_items = []
    len_of_queue = bin_heap2.get_size()
    for i in range(0, len_of_queue):
        removed_items.append(bin_heap2.remove_min())

    assert sample_data_ordered == removed_items
