from ..factorial import *


def test_fib_loop():
    assert factorial(10) == 3628800


def test_fib_recursion():
    assert factorial_with_recursion(10) == 3628800
