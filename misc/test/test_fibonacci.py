from ..fibonacci import *


def test_fibonacci():
    assert fibonacci(10) == 89


def test_fibonacci_with_recursion():
    assert fibonacci_with_recursion(10) == 89


def test_fibonacci_with_dynamic_programming():
    assert fibonacci_with_dynamic_programming(10) == 89
