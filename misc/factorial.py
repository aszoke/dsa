"""
Factorial module
"""


def factorial(num: int) -> int:
    """
    **FACTORIAL** calculates the number's fibonacci value within a loop.
    :param num:
        input number
    :return:
        factorial value
    """
    result = 1
    for i in range(2, num + 1):
        result = result * i
    return result


def factorial_with_recursion(num: int) -> int:
    """
    **FACTORIAL WITH RECURSION** calculates the number's fibonacci value with recursion.
    :param num:
        input number
    :return:
        factorial value
    """
    if num == 1:
        return 1
    else:
        return num * factorial_with_recursion(num - 1)
