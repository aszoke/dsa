"""
Fibonacci module
"""


# Recursion is an effective problem-solving technique. It could be called as _functional_ programming style. When you
# program in the functional style, you think much more about the definition of _what_ you are programming than _how_
# you are going to program it. Some say that writing recursive functions is a _declarative_ approach rather than an
# _imperative_ approach.

# A recursive function is simply a function that calls itself.
# Recursion properties:
#   1) All recursive algorithms must have a base case - which returns a value.
#   2) A recursive algorithm must change its state and make progress toward the base case.
#   3) A recursive algorithm must call itself (recursively).
#
# - Recursion can take the place of iteration in some cases. (Iterations are replaced by recursions!) - Recursive
# algorithms take at least 𝑂(𝑛) space, where num is the depth of the recursive call. - All recursive algorithms _can_
# be implemented iteratively, although they may be much more complex. - Recursion is not always the answer. Sometimes
# a recursive solution may be more computationally expensive than an alternative algorithm. - Recursion can be
# applied to lots of different problems including sorting, searching, - Recursion - as it based on function calls -
# uses the STACK to slice the data and build the solution using these data particles when the call returns!!!

# The Run-Time Stack and the Heap: The run-time stack is a stack of Activation Records. The Python interpreter
# _pushes_ an activation record onto the run-time stack when a function is called. When a function returns the Python
# interpreter _pops_ the corresponding activation record off the run-time stack. Python stores the identifiers
# defined in the local scope in an activation record. The Heap is the area of RAM where all objects are stored. When
# an object is created it resides in the heap. The run-time stack never contains objects. References to objects are
# stored within the run-time stack and those references point to objects in the heap.

# LEGB: standing for Local, Enclosing, Global, and Built-In can help you memorize the rules of scope in Python. The
# order of the letters in the acronym is important. When the Python interpreter encounters an identifier in a
# program, it searches the local scope first, followed by all the enclosing scopes from the inside outward,
# followed by the global scope, and finally the built-in scope.

# See: https://realpython.com/python-thinking-recursively/


def fibonacci(num: int) -> int:
    """
    **FIBONACCI** calculates the number's fibonacci value within a loop.
    :param num:
        input number
    :return:
        fibonacci value

         F(0) = 1
         F(1) = 1
         F(2) = F(1) + F(0)
         F(3) = F(2) + F(1)
         ...
         F(num) = F(num-1) + F(num-2)
         1, 1, 2, 3, 5, 8, 13, ...

    """
    f_prev = 1
    f_curr = 1
    for _ in range(2, num + 1):
        tmp = f_curr
        f_curr = f_curr + f_prev
        f_prev = tmp

    return f_curr


def fibonacci_with_recursion(num: int) -> int:
    """
    **FIBONACCI WITH RECURSION** calculates the number's fibonacci value wit recursion.
    :param num:
        input number
    :return:
        fibonacci value

                                   fib(5)
                              /                \
                        fib(4)                fib(3)
                      /        \\              /       \
                  fib(3)      fib(2)         fib(2)   fib(1)
                 /    \\       /    \\        /      \
           fib(2)   fib(1)  fib(1) fib(0) fib(1) fib(0)
           /     \
         fib(1) fib(0)

    **Complexity**
        Time Complexity: T(num) = T(num-1) + T(num-2) which is exponential.
    """
    # trace
    # print(str(num), end=' ')
    if num in (0, 1):
        return 1
    else:
        return fibonacci_with_recursion(num - 1) + fibonacci_with_recursion(num - 2)


# Fibonacci: calculating with recursion (using the closed formulae)
memo = {0: 1, 1: 1}


def fibonacci_with_dynamic_programming(num: int) -> int:
    """
    **FIBONACCI WITH DYNAMIC PROGRAMMING** calculates the number's fibonacci value with dynamic programming
    (recursiont + memoization).

    Memoization is an interesting programming technique that can be employed when  you write functions that may get
    called more than once with the same arguments. The idea behind memoization is to do the work of computing a value
    in a function once. Then, we make a note to ourselves so when the function is called with the same arguments
    again, we return the value we just computed again. This avoids going to the work of computing the value all over
    again.

    :param num:
        input number
    :return: fibonacci value
    """
    if num in (0, 1):
        return 1
    else:
        if memo.get(num - 1) is None:
            memo[num - 1] = fibonacci_with_dynamic_programming(num - 1)
        elif memo.get(num - 2) is None:
            memo[num - 2] = fibonacci_with_dynamic_programming(num - 2)

    return memo.get(num - 1) + memo.get(num - 2)
