import pytest

from ..bfs import bfs


@pytest.fixture
def sample_graph():
    graph = {
        'A': ['B', 'C'],
        'B': ['D', 'E'],
        'C': ['F', 'G'],
        'D': ['H'],
        'E': ['I', 'J'],
        'F': ['K'],
        'G': [],
        'H': [],
        'I': [],
        'J': [],
        'K': []
    }
    init_node = 'A'
    bfs_traversal = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K']
    return graph, init_node, bfs_traversal


def test_dfs_recursive_traversal(sample_graph):
    graph = sample_graph[0]
    init_node = sample_graph[1]
    traversal = sample_graph[2]
    assert bfs(graph, init_node) == traversal
