import pytest

from ..dfs import dfs


@pytest.fixture
def sample_graph():
    graph = {
        'A': ['B', 'C'],
        'B': ['D', 'E'],
        'C': ['F', 'G'],
        'D': ['H'],
        'E': ['I', 'J'],
        'F': ['K'],
        'G': [],
        'H': [],
        'I': [],
        'J': [],
        'K': []
    }
    init_node = 'A'
    dfs_traversal = ['A', 'B', 'D', 'H', 'E', 'I', 'J', 'C', 'F', 'K', 'G']
    return graph, init_node, dfs_traversal


def test_dfs_traversal(sample_graph):
    graph = sample_graph[0]
    init_node = sample_graph[1]
    traversal = sample_graph[2]
    assert dfs(graph, init_node) == traversal
