"""
Breath-first search (BFS) module
"""

bfs_visited = set()  # keep tracking of those nodes that are visited to avoid re-visitation
queue = []       # bin_heap is a list that is used to keep track of the visitation order <-- SINCE BFS therefore QUEUE!
traverse = []


def bfs(graph: dict, node: str) -> list:
    """
    **BREATH-FIRST SEARCH (BFS)** is an algorithm for tree traversal on graph or tree data structures.

    The Algorithm
    1. Pick any node, visit the adjacent unvisited vertex, mark it as visited, display it, and insert it in a bin_heap.
    2. If there are no remaining adjacent vertices left, _remove the first vertex from the bin_heap.
    3. Repeat step 1 and step 2 until the bin_heap is empty or the desired node is found.

    :param graph:
        input graph
    :param node:
        initial node

    **Complexity**
    time complexity: O(V+E), where V is the number of vertices and E is the number of edges

    **See also**
    :mod:`graph.dfs_recursive`
    """

    queue.append(node)       # put the node into the bin_heap that have to be visited

    # Then, while the bin_heap contains elements, it keeps taking out nodes from the bin_heap, appends the neighbors
    # of that node to the bin_heap if they are unvisited, and marks them as visited. This continues until the
    # bin_heap is empty.
    while queue:
        # Dequeue a vertex from bin_heap and print it
        item = queue.pop(0)  # POP the FIRST element from the QUEUE
        traverse.append(item)

        # Get all adjacent vertices of the dequeued vertex s. If a adjacent has not been visited, then mark it
        # visited and enqueue it
        for neighbour in graph[item]:
            if neighbour not in bfs_visited:
                bfs_visited.add(neighbour)
                queue.append(neighbour)

    return traverse
