"""
Depth-first search (DFS) recursive module
"""

dfs_visited = set()  # keep tracking of those nodes that are visited to avoid re-visitation
traverse = []


def dfs_recursive(graph: dict, node: str) -> list:
    """
    **DEPTH-FIRST SEARCH (DFS)** is an algorithm for tree traversal on graph or tree data structures.

    :param graph:
        input graph
    :param node:
        initial node
    :return:
        trajectory of the traversal

    **Complexity**
    time complexity: O(V+E), where V is the number of vertices and E is the number of edges

    **See also**
    :mod:`graph.dfs`
    """

    if node not in dfs_visited:  # NOTE: the base case is implicit
        traverse.append(node)
        # It first checks if the current node is unvisited - if yes, it is appended in the visited set.
        dfs_visited.add(node)
        for neighbour in graph[node]:  # Then for each neighbor of the current node, the dfs function is invoked again.
            dfs_recursive(graph, neighbour)  # The base case is invoked when all the nodes are visited then it returns.

    if len(traverse) == len(dfs_visited):
        return traverse
