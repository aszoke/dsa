"""
Depth-first search (DFS) iterative module
"""

dfs_visited = set()     # keep tracking of those nodes that are visited to avoid re-visitation
stack = []     # stack is a list that is used to keep track of the visitation order <-- since DFS therefore a STACK
traverse = []


def dfs(graph: dict, node: str) -> list:
    """
    **DEPTH-FIRST SEARCH (DFS)** is an algorithm for tree traversal on graph or tree data structures.

    The Algorithm
    1. Pick any node. If it is unvisited, mark it as visited and recur on all its adjacent nodes.
    2. Repeat until all the nodes are visited, or the node to be searched is found.

    :param graph:
        input graph
    :param node:
        initial node
    :return:
        trajectory of the traversal

    **Complexity**
    time complexity: O(V+E), where V is the number of vertices and E is the number of edges

    **See also**
    :mod:`graph.dfs_recursive`
    """
    stack.insert(0, node)
    while stack:
        val = stack.pop(0)
        traverse.append(val)

        # for RESERVED order storing! in order to simulate the LIFO operation! to simulate RIGHT-TO-LEFT traversal
        for neighbour in reversed(graph[val]):
            if neighbour not in dfs_visited:
                dfs_visited.add(neighbour)
                stack.insert(0, neighbour)

    return traverse
