"""
Binary search tree module
"""


class BinarySearchTree:
    """
    **BINARY SEARCH TREE SORT** is a node-based binary tree data structure for searching.

    A binary search tree (BST), also called an ordered or sorted binary tree, is a rooted binary tree whose internal
    nodes each store a key greater than all the keys in the node's left subtree and less than those in its right
    subtree. Also the left and right subtrees must also be binary search trees.

    A binary tree is a type of data structure for storing data such as numbers in an organized way. Binary search trees
    allow binary search for fast lookup, addition and removal of data items, and can be used to implement dynamic sets
    and lookup tables.

    Binary search trees are of some academic interest. However, they are not used
    much in practice.

    **Complexity**

    In the average case, inserting into a binary search tree takes O(log nu)
    time. To insert num items into a binary search tree would take O(num log num) time. So, in
    the average case we have an algorithm for sorting a sequence of ordered items.
    However, it takes more space than a list and the quicksort algorithm can sort a list
    with the same big-Oh complexity. In the worst case, binary search trees suffer from
    the same problem that quicksort suffers from. When the items are already sorted,
    both quicksort and binary search trees perform poorly. The complexity of inserting
    num items into a binary search tree becomes O(num^2) in the worst case. The tree becomes
    a stick if the values are already sorted and essentially becomes a linked list.

    There are a couple of nice properties of binary search trees that a random access
    list does not have. Inserting into a tree can be done in O(log num) time in the average case
    while inserting into a list would take O(num) time. Deleting from a binary search tree
    can also be done in O(log num) time in the average case. Looking up a value in a binary
    search tree can also be done in O(log num) time in the average case. If we have lots of
    insert, delete, and lookup operations for some algorithm, a tree-like structure may be
    useful. But, binary search trees cannot guarantee the O(log num) complexity. It turns out
    that there are implementations of search tree structures that can guarantee O(log num)
    complexity or better for inserting, deleting, and searching for values.

    **See also**

    Splay Trees, AVL-Trees, and B-Trees.
    """

    class Node:
        """
        Datatype for BinarySearchTree
        """
        def __init__(self, val: int, left=None, right=None):
            self.val = val
            self.left = left
            self.right = right

        def get_val(self):
            return self.val

        def set_val(self, new_val):
            self.val = new_val

        def get_left(self):
            return self.left

        def get_right(self):
            return self.right

        def set_left(self, new_left):
            self.left = new_left

        def set_right(self, new_right):
            self.right = new_right

        def __iter__(self):
            """It does an inorder traversal of the nodes of the tree yielding all the values. In this way, we get
            the values in ascending order."""
            if self.left is not None:
                for elem in self.left:
                    yield elem

            yield self.val

            if self.right is not None:
                for elem in self.right:
                    yield elem

        def __repr__(self) -> str:
            """Python __repr__() function returns the object representation. It could be any valid python expression
            such as tuple, dictionary, string etc. This method is called when repr() function is invoked on the object,
            in that case, __repr__() function must return a String otherwise error will be thrown."""
            return "BinarySearchTree.Node(" + repr(self.val) + "," + repr(self.left) + "," + repr(self.right) + ")"

    # Below are the methods of the BinarySearchTree class.
    def __init__(self, root=None):
        self.root = root

    def insert(self, val: int):
        self.root = BinarySearchTree.__insert(self.root, val)

    def get_array(self):
        lst = []
        for i in self.root:
            lst.append(i)

        return lst

    @staticmethod
    def __insert(root: Node, val):
        """The __insert function is recursive and is not passed a self parameter. It is a static function (not a
        method of the class) but is hidden inside the insert function so users of the class will not know it exists."""

        if root is None:
            return BinarySearchTree.Node(val)

        if val < root.get_val():
            # left side of the BST is actually a BST! -> so it is a recursive call: the left node will be the node in
            # which it tries to insert the node.get_left() returns the left BST tree
            root.set_left(BinarySearchTree.__insert(root.get_left(), val))
        else:
            # right side of the BST is actually a BST! -> so it is a recursive call: the right node will be the node
            # in which it tries to insert the node.get_left() returns the left BST tree
            root.set_right(BinarySearchTree.__insert(root.get_right(), val))

        return root

    def __iter__(self):
        """a Python iterator object must implement two special methods, __iter__() and __next__(), collectively
        called the **iterator protocol**"""
        if self.root is not None:
            return iter(self.root)
        else:
            return iter([])

    def __str__(self):
        """This method returns the string representation of the object. This method is called when print() or str()
        function is invoked on an object."""
        return "BinarySearchTree(" + repr(self.root) + ")"
