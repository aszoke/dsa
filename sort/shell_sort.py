"""
Shell sort module
"""


def shell_sort(a_list: list) -> list:
    """
    **SHELL SORT** is mainly a variation of Insertion Sort.

    It sorts by breaking the original list into a number of smaller sublists, each of which is sorted using
    an insertion sort. The unique way that these sublists are chosen is the key to the shell sort.
    Instead of breaking the list into sublists of contiguous items, the shell sort uses an increment i,
    sometimes called the gap, to create a sublist by choosing all items that are i items apart.
    Note that by performing the earlier sublist sorts, we have now reduced the total
    number of shifting operations necessary to put the list in its final order. For this case, we need
    only four more shifts to complete the process.

    :param a_list: input list that should be ordered
    :return: ordered list

    **Complexity**
    O(num^(3/2))


    """
    sublist_count = len(a_list) // 2
    while sublist_count > 0:
        for start_position in range(sublist_count):
            _gap_insertion_sort(a_list, start_position, sublist_count)
        # print("After increments of size", sublist_count, "The list is", a_list)
        sublist_count = sublist_count // 2
    return a_list


def _gap_insertion_sort(a_list, start, gap):
    for i in range(start + gap, len(a_list), gap):
        current_value = a_list[i]
        position = i
        while position >= gap and a_list[position - gap] > current_value:
            a_list[position] = a_list[position - gap]
            position = position - gap
        a_list[position] = current_value
