"""
Bubble sort module
"""


def bubble_sort(a_list: list) -> list:
    """**BUBBLE SORT** is the simplest sorting algorithm that works by repeatedly swapping the adjacent
    elements if they are in wrong order.

    It compares adjacent items and exchanges those that are out of order. Each pass through the list places the next
    largest value in its proper place. In essence, each item “bubbles” up to the location where it belongs.

    :param a_list: input list that should be ordered
    :return: ordered list

    **Complexity**
    O(num^2)

    **See also**
    :mod:`sort.bubble_sort_v2, sort.bubble_sort_v3`
    """

    for _ in range(len(a_list) - 1):

        for j in range(len(a_list) - 1):

            if a_list[j] > a_list[j + 1]:
                swap = a_list[j]
                a_list[j] = a_list[j + 1]
                a_list[j + 1] = swap

    return a_list
