"""
Insertion sort module
"""


def insertion_sort(a_list: list) -> list:
    """
    **INSERTION SORT** is a simple sorting algorithm that works similar to the way you sort playing
    cards in your hands.

    The array is virtually split into a sorted and an unsorted part. Values from the unsorted part are picked and
     placed at the correct position in the sorted part.

    One note about shifting versus exchanging is also important. In general, a shift operation requires
    approximately a third of the processing work of an exchange since only one assignment is performed.

    :param a_list: input list that should be ordered
    :return: ordered list

    **Complexity**
    O(num^2)

    **See also**
    :mod:`sort.insertion_sort_recursive`
    """
    for index in range(1, len(a_list)):

        position = index
        current_value = a_list[position]

        while position > 0 and a_list[position - 1] > current_value:
            a_list[position] = a_list[position - 1]
            position = position - 1
            # print(' '.join([str(key) for key in a_list]))

        a_list[position] = current_value

    return a_list
