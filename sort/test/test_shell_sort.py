import pytest

from sort import shell_sort as she_sort
from . import data


@pytest.mark.parametrize("input_params", data.list_case)
def test_shell_sort_list(input_params):
    assert she_sort.shell_sort(input_params) == sorted(input_params)


@pytest.mark.parametrize("input_params", data.ordered_list_case)
def test_shell_sort_odered_list(input_params):
    assert she_sort.shell_sort(input_params) == sorted(input_params)


def test_shell_sort_huge_list(random_list):
    assert she_sort.shell_sort(random_list) == sorted(random_list)


def test_shell_sort_huge_ordered_list(random_ordered_list):
    assert she_sort.shell_sort(random_ordered_list) == sorted(random_ordered_list)
