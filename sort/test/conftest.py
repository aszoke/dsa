"""
Pytest configuration file for hooks.
"""
import os
import pstats
import random
from typing import List

import pytest


def _generate_data_set_helper():
    _size: int = 100

    _rnd_list: List[int] = [random.randint(0, _size) for _ in range(_size)]

    return _rnd_list


@pytest.fixture(scope="session")
def random_list():
    _random_list = _generate_data_set_helper()
    ret = _random_list
    return ret


@pytest.fixture(scope="session")
def random_ordered_list():
    _random_list = _generate_data_set_helper()
    ret = sorted(_random_list)
    return ret


def pytest_configure(config):
    """
    Allows plugins and conftest files to perform initial configuration. This hook is called for every plugin and
    initial conftest file after command line options have been parsed.
    """
    print("\n[Pytest phase 1/4]: pytest_configure")


def pytest_sessionstart(session):
    """
    Called after the Session object has been created and before performing collection and entering the run test loop.
    """
    print("\n[Pytest phase 2/4]: pytest_sessionstart")


def pytest_sessionfinish(session, exitstatus):
    """
    Called after whole test run finished, right before returning the exit status to the system.
    """
    print("\n[Pytest phase 3/4]: pytest_sessionfinish")


def pytest_unconfigure(config):
    """
    Called before test process is exited.
    """
    print("\n[Pytest phase 4/4]: pytest_unconfigure")

    app_dir = os.getcwd()
    print("*** Pytest running directory is:", app_dir, "***")

    profile_file = ".profile_report/combined.prof"
    print("*** Pytest profile file is:", profile_file, "***")

    if os.path.exists(profile_file):
        prst = pstats.Stats(profile_file).sort_stats("cumulative", "module")
        prst.strip_dirs()
        # TODO CHANGE the FILTER to the appropriate value when you add a new module
        filename_filter = "sort"
        prst.print_stats("^(?!test).*_" + filename_filter + ".*$")
    else:
        print("*** Pytest profile file is not available. Profile stats are not shown. ***")
