import pytest

from sort import selection_sort_recursive as sel_sort
from . import data


@pytest.mark.parametrize("input_params", data.list_case)
def test_selection_sort_list(input_params):
    assert sel_sort.selection_sort_recursive(input_params) == sorted(input_params)


@pytest.mark.parametrize("input_params", data.ordered_list_case)
def test_selection_sort_odered_list(input_params):
    assert sel_sort.selection_sort_recursive(input_params) == sorted(input_params)


def test_selection_sort_huge_list(random_list):
    assert sel_sort.selection_sort_recursive(random_list) == sorted(random_list)


def test_selection_sort_huge_ordered_list(random_ordered_list):
    assert sel_sort.selection_sort_recursive(random_ordered_list) == sorted(random_ordered_list)
