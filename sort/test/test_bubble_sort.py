import pytest

from sort import bubble_sort as bub_sort
from . import data


@pytest.mark.parametrize("input_params", data.list_case)
def test_bubble_sort_list(input_params):
    assert bub_sort.bubble_sort(input_params) == sorted(input_params)


@pytest.mark.parametrize("input_params", data.ordered_list_case)
def test_bubble_sort_odered_list(input_params):
    assert bub_sort.bubble_sort(input_params) == sorted(input_params)


def test_bubble_sort_huge_list(random_list):
    assert bub_sort.bubble_sort(random_list) == sorted(random_list)


def test_bubble_sort_huge_ordered_list(random_ordered_list):
    assert bub_sort.bubble_sort(random_ordered_list) == sorted(random_ordered_list)
