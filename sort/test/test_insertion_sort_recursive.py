import pytest

from sort import insertion_sort_recursive as ins_sort
from . import data


@pytest.mark.parametrize("input_params", data.list_case)
def test_insertion_sort_list(input_params):
    assert ins_sort.insertion_sort_recursive(input_params) == sorted(input_params)


@pytest.mark.parametrize("input_params", data.ordered_list_case)
def test_binsertion_sort_odered_list(input_params):
    assert ins_sort.insertion_sort_recursive(input_params) == sorted(input_params)


def test_insertion_sort_huge_list(random_list):
    assert ins_sort.insertion_sort_recursive(random_list) == sorted(random_list)


def test_insertion_sort_huge_ordered_list(random_ordered_list):
    assert ins_sort.insertion_sort_recursive(random_ordered_list) == sorted(random_ordered_list)
