import pytest

from sort.binary_search_tree_sort import BinarySearchTree
from . import data


@pytest.fixture
def tree_case_list_0():
    lst_case = data.list_case[0]

    ordered_tree = BinarySearchTree()
    for item in lst_case:
        ordered_tree.insert(item)

    return ordered_tree, sorted(lst_case)


def test_bst_sort_list_0(tree_case_list_0):
    ordered, expected = tree_case_list_0
    assert ordered.get_array() == expected


@pytest.fixture
def tree_case_list_1():
    lst_case = data.list_case[1]

    ordered_tree = BinarySearchTree()
    for item in lst_case:
        ordered_tree.insert(item)

    return ordered_tree, sorted(lst_case)


def test_bst_sort_list_1(tree_case_list_1):
    ordered, expected = tree_case_list_1
    assert ordered.get_array() == expected


@pytest.fixture
def tree_case_ordered_list_0():
    lst_case = data.ordered_list_case[0]

    ordered_tree = BinarySearchTree()
    for item in lst_case:
        ordered_tree.insert(item)

    return ordered_tree, sorted(lst_case)


def test_bst_sort_ordered_list_0(tree_case_ordered_list_0):
    ordered, expected = tree_case_ordered_list_0
    assert ordered.get_array() == expected


@pytest.fixture
def tree_case_ordered_list_1():
    lst_case = data.ordered_list_case[1]

    ordered_tree = BinarySearchTree()
    for item in lst_case:
        ordered_tree.insert(item)

    return ordered_tree, sorted(lst_case)


def test_bst_sort_ordered_list_1(tree_case_ordered_list_1):
    ordered, expected = tree_case_ordered_list_1
    assert ordered.get_array() == expected


def test_bst_sort_huge_list(random_list):
    ordered_tree = BinarySearchTree()

    for i in random_list:
        ordered_tree.insert(i)

    assert ordered_tree.get_array() == sorted(random_list)


def test_bst_sort_huge_ordered_list(random_ordered_list):
    ordered_tree = BinarySearchTree()

    for i in random_ordered_list:
        ordered_tree.insert(i)

    assert ordered_tree.get_array() == sorted(random_ordered_list)
