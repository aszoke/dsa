import pytest

from sort import merge_sort as mer_sort
from . import data


@pytest.mark.parametrize("input_params", data.list_case)
def test_merge_sort_list(input_params):
    assert mer_sort.merge_sort(input_params) == sorted(input_params)


@pytest.mark.parametrize("input_params", data.ordered_list_case)
def test_merge_sort_odered_list(input_params):
    assert mer_sort.merge_sort(input_params) == sorted(input_params)


def test_merge_sort_huge_list(random_list):
    assert mer_sort.merge_sort(random_list) == sorted(random_list)


def test_merge_sort_huge_ordered_list(random_ordered_list):
    assert mer_sort.merge_sort(random_ordered_list) == sorted(random_ordered_list)
