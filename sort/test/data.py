"""
 Data sets (input and expected outputs) for tests
"""
list_case = [
    [1, 2, 32, 8, 17, 19, 42, 13, 0],
    [92, 72, 39, 21, 52, 75, 35, 53, 41, 25, 11, 17, 24, 22, 57, 56, 48, 60, 81, 26, 19, 74,  7, 88, 91,  5, 18, 63, 13,
     12, 82, 62, 67, 14, 76, 8]
]

ordered_list_case = [
    [0, 1, 2, 8, 13, 17, 19, 32, 42],
    [5, 7, 8, 11, 12, 13, 14, 17, 18, 19, 21, 22, 24, 25, 26, 35, 39, 41, 48, 52, 53, 56, 57, 60, 62, 63, 67, 72, 74,
     75, 76, 81, 82, 88, 91, 92]
]
