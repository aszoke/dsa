import pytest

from sort import insertion_sort as ins_sort
from . import data


@pytest.mark.parametrize("input_params", data.list_case)
def test_insertion_sort_list(input_params):
    assert ins_sort.insertion_sort(input_params) == sorted(input_params)


@pytest.mark.parametrize("input_params", data.ordered_list_case)
def test_insertion_sort_odered_list(input_params):
    assert ins_sort.insertion_sort(input_params) == sorted(input_params)


def test_insertion_sort_huge_list(random_list):
    assert ins_sort.insertion_sort(random_list) == sorted(random_list)


def test_insertion_sort_huge_ordered_list(random_ordered_list):
    assert ins_sort.insertion_sort(random_ordered_list) == sorted(random_ordered_list)
