"""
Merge sort module
"""


def merge_sort(a_list: list) -> list:
    """
    **MERGE SORT** is a Divide and Conquer-based sort algorithm.

    Merge sort is a recursive algorithm that continually splits a list in half. If the list is empty or has one
     item, it is sorted by definition (the base case). If the list has more than one item,
    we split the list and recursively invoke a merge sort on both halves. Once the two halves are sorted, the
    fundamental operation, called a merge, is performed. Merging is the process of taking two
    smaller sorted lists and combining them together into a single, sorted, new list.

    :param a_list: input list that should be ordered
    :return: ordered list

    **Complexity**
    O(num log num)
    """
    if len(a_list) > 1:
        mid = len(a_list) // 2
        left_half = a_list[:mid]
        right_half = a_list[mid:]

        merge_sort(left_half)
        merge_sort(right_half)

        i = 0  # left index
        j = 0  # right index
        k = 0  # merged list index

        # merge until the left and right have elements
        while i < len(left_half) and j < len(right_half):
            if left_half[i] < right_half[j]:
                a_list[k] = left_half[i]
                i = i + 1
            else:
                a_list[k] = right_half[j]
                j = j + 1
            k = k + 1

        while i < len(left_half):
            a_list[k] = left_half[i]
            i = i + 1
            k = k + 1

        while j < len(right_half):
            a_list[k] = right_half[j]
            j = j + 1
            k = k + 1

    return a_list
