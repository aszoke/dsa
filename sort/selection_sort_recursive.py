"""
Selection sort recursive module
"""


def selection_sort_recursive(a_list: list) -> list:
    """
    **SELECTION SORT RECURSIVE** is a basic sort algorithm.

    :param a_list: input list that should be ordered
    :return: ordered list

    **Complexity**
    O(num^2)

    **See also**
    :mod:`sort.selection_sort`
    """

    return _selection_sort_core(a_list, len(a_list)-1)


def _selection_sort_core(seq, i):
    if i == 0:
        return seq     # Base case -- do nothing

    max_j = i  # Idx. of largest value so far
    for j in range(i):  # Look for a larger value
        if seq[j] > seq[max_j]:
            max_j = j  # Found one? Update max_j

    seq[i], seq[max_j] = seq[max_j], seq[i]  # Switch largest into place

    return _selection_sort_core(seq, i - 1)  # Sort 0..i-1
