"""
Bubble sort module v3
"""


def bubble_sort(a_list: list) -> list:
    """
    **BUBBLE SORT** sort is a simple sorting algorithm - Enhanced version 3

    :param a_list: input list that should be ordered
    :return: ordered list

    **Complexity**
    O(num^2)

    **See also**
    :mod:`sort.bubble_sort, sort.bubble_sort_v2`
    """

    exchanged = True    # exchanged: if no more exchanges then the rest of the list is ordered
    pass_num = len(a_list) - 1
    while pass_num > 0 and exchanged:
        exchanged = False
        for j in range(pass_num):
            if a_list[j] > a_list[j + 1]:
                exchanged = True
                swap = a_list[j]
                a_list[j] = a_list[j + 1]
                a_list[j + 1] = swap
        pass_num = pass_num - 1
    return a_list
