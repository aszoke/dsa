"""
Selection sort module
"""


def selection_sort(a_list: list) -> list:
    """
    **SELECTION SORT** is a basic sort algorithm.

    A selection sort looks for the largest value as it makes a pass and, after completing the pass, places it to the
     end of the list.

    :param a_list: input list that should be ordered
    :return: ordered list

    **Complexity**
    O(num^2)

    **See also**
    :mod:`sort.selection_sort_recursive`
    """
    pass_num = len(a_list) - 1
    while pass_num > 0:
        exchange = False
        max_item_index = pass_num
        for j in range(0, pass_num, 1):
            if a_list[j] > a_list[max_item_index]:
                exchange = True
                max_item_index = j
        if exchange:
            swap = a_list[max_item_index]
            a_list[max_item_index] = a_list[pass_num]
            a_list[pass_num] = swap
            # print(' '.join([str(key) for key in a_list]))
        pass_num = pass_num - 1
    return a_list
