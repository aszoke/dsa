"""
Bubble sort module v2
"""


def bubble_sort(a_list: list) -> list:
    """
    **BUBBLE SORT** sort is a simple sorting algorithm - Enhanced version 2

    :param a_list: input list that should be ordered
    :return: ordered list

    **Complexity**
    O(num^2)

    **See also**
    :mod:`sort.bubble_sort, sort.bubble_sort_v3`
    """

    cycle = 0
    for _ in range(len(a_list) - 1):
        # cycle: in every cycle the last list is in its right place (i.e. ordered) so the inner cycle
        # could be shortened
        for j in range(len(a_list) - 1 - cycle):
            if a_list[j] > a_list[j + 1]:
                swap = a_list[j]
                a_list[j] = a_list[j + 1]
                a_list[j + 1] = swap
        cycle = cycle + 1

    return a_list
