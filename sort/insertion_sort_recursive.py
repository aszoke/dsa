"""
Insertion sort recursive module
"""


def insertion_sort_recursive(a_list: list) -> list:
    """
    **INSERTION SORT RECURSIVE** is a simple sorting algorithm that works similar to the way you sort
    playing cards in your hands.

    :param a_list: input list that should be ordered
    :return: ordered list

    **See also**
    :mod:`sort.insertion_sort`
    """

    return _insertion_sort_core(a_list, len(a_list)-1)


def _insertion_sort_core(seq, i):
    if i == 0:
        return seq  # Base case -- do nothing

    val = _insertion_sort_core(seq, i - 1)  # Sort 0..i-1
    j = i  # Start "walking" down

    while j > 0 and seq[j - 1] > seq[j]:  # Look for OK spot
        seq[j - 1], seq[j] = seq[j], seq[j - 1]  # Swap, keep moving seq[j] down
        j -= 1  # decrement of j

    return val
