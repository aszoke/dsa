"""
Quick sort module
"""


def quick_sort(a_list: list) -> list:
    """
    **QUICK SORT** is a Divide and Conquer-based sort algorithm.

    Like Merge Sort, QuickSort is a Divide and Conquer algorithm. It picks an element as pivot and partitions the
    given array around the picked pivot. There are many different versions of quickSort that pick pivot in
    different ways.

    O(num log num) But may degrade to 𝑂(𝑛2) if the split points are not near the middle of the list. The quick sort
    uses divide and conquer to gain the same advantages as the merge sort, while not using additional storage as the
    merge sort.

    :param a_list: input list that should be ordered
    :return: ordered list

    **Complexity**
    O(num log num)
    """
    _quick_sort_helper(a_list, 0, len(a_list) - 1)

    return a_list


def _quick_sort_helper(a_list, first, last):
    if first < last:
        split_point = _partition(a_list, first, last)

        _quick_sort_helper(a_list, first, split_point - 1)
        _quick_sort_helper(a_list, split_point + 1, last)


def _partition(a_list, first, last):
    """
    The key process in quickSort is partition(). Target of partitions is, given an array and an element x of array
    as pivot, put x at its correct position in sorted array and put all smaller elements (smaller than x) before x,
    and put all greater elements (greater than x) after x. All this should be done in linear time.
    """
    pivot_value = a_list[first]

    left_mark = first + 1
    right_mark = last

    done = False
    while not done:

        while left_mark <= right_mark and a_list[left_mark] <= pivot_value:
            left_mark = left_mark + 1

        while a_list[right_mark] >= pivot_value and right_mark >= left_mark:
            right_mark = right_mark - 1

        if right_mark < left_mark:
            done = True
        else:
            temp = a_list[left_mark]
            a_list[left_mark] = a_list[right_mark]
            a_list[right_mark] = temp

        temp = a_list[first]
        a_list[first] = a_list[right_mark]
        a_list[right_mark] = temp

    return right_mark
