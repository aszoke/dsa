# DSA (Data structures and algorithms)

## Description

DSA is a Python project that allows developers to study basic data structures and algorithms in Python. Some algorithms are from a book Problem Solving with Algorithms and Data Structures using Python. Some algorithms are from a book [Problem Solving with Algorithms and Data Structures using Python](https://runestone.academy/runestone/books/published/pythonds/index.html).

## Features

* search algorithms
* sort algorithms
* tree algorithms
* graph algorithms

## Prerequisites

* Python 3.7

Module's package requirements can be found in ```requirements.txt```

## Installation

Use the git clone function to create a local repository.

```
git clone https://aszoke@bitbucket.org/aszoke/dsa.git
```

## Verification

See ```pytest.ini``` and ```conftest.py``` files for test configurations and ```.coveragerc``` for coverage 
configuration.

In the project root directory run the following command (includes profiling!):

```
pytest . -v --profile --pstats-dir=.profile_report
```

## Additional dependency information

### List project dependencies

To list dependencies in a tree-like form, run the ```pipdeptree``` command.

### Generate project dependencies from command line

```
pip freeze > requirements.txt
```

## Test reports

### Basic test config

In the project root directory run the following command (includes profiling!):

```
pytest . -v --profile --pstats-dir=.profile_report
```

For code coverage run the following:

```
coverage run -m pytest . -v --profile --pstats-dir=.profile_report

coverage report -m
```

### Detailed test configuration (conftest.py)

To get the detailed configuration steps, in the project root directory, run the following command:

```
pytest . -v --setup-show
```

### Test report for CI

This report can be which can be read by Jenkins or other Continuous integration servers.

```
pytest . -v --junitxml=.junit_report/junit_report.xml
```

## Quality reports

### Profiling packages

The ```conftest.py``` contains pytest profiling configurations to profile tests.

### Linting packages

The ```.pylintrc.py``` contains linting configuration options. 

Linting report could be generated with the following command:

```
pylint --rcfile=.pylintrc sort search knapsack tree heap graph hash misc <package names>
```

### Code metrics

The ```.radon.cfg``` contains linting configuration options. 

Code metrics report could be generated with the following command:

```
radon mi sort search knapsack tree heap graph hash misc <package names>
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
