"""
Binary tree list-based module
"""


def binary_tree(root: int) -> list:
    """
    **BINARY TREE - LIST BASED** binary tree implemented as nested lists

    :param root: node value
    :return: list contains the node and its left and right children

    **See also**
    :mod:`binary_tree.BinaryTree`
    """
    return [root, [], []]


def insert_left(node: list, new_branch: int) -> list:
    tree = node.pop(1)
    if len(tree) > 1:
        node.insert(1, [new_branch, tree, []])
    else:
        node.insert(1, [new_branch, [], []])
    return node


def insert_right(node: list, new_branch: int) -> list:
    tree = node.pop(2)
    if len(tree) > 1:
        node.insert(2, [new_branch, [], tree])
    else:
        node.insert(2, [new_branch, [], []])
    return node


def get_root_val(root: list) -> list:
    return root[0]


def set_node_val(node: list, new_val: list):
    node[0] = new_val


def get_left_child(node: list) -> list:
    return node[1]


def get_right_child(node: list) -> list:
    return node[2]
