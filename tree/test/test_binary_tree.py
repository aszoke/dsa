from ..binary_tree import BinaryTree


def test_binary_tree_insert_root():
    root_val = '1'
    bt = BinaryTree(root_val)

    assert bt.get_node_val() == root_val


def test_binary_tree_insert_left():
    root_val = '1'
    left_val = '1.1'
    bt = BinaryTree(root_val)

    assert bt.insert_left_child(left_val).get_node_val() == left_val


def test_binary_tree_insert_right():
    root_val = '1'
    right_val = '1.1'
    bt = BinaryTree(root_val)

    assert bt.insert_right_child(right_val).get_node_val() == right_val


def test_binary_tree_get_left():
    root_val = '1'
    left_val = '1.1'
    bt = BinaryTree(root_val)
    bt.insert_left_child(left_val)

    assert bt.get_left_child_val() == left_val


def test_binary_tree_get_right():
    root_val = '1'
    right_val = '1.1'
    bt = BinaryTree(root_val)
    bt.insert_right_child(right_val)

    assert bt.get_right_child_val() == right_val


def test_binary_tree_get_left_left():
    root_val = '1'
    left_val = '1.1'
    left_left_val = '1.1.1'
    bt = BinaryTree(root_val)
    bt_left = bt.insert_left_child(left_val)
    bt_left.insert_left_child(left_left_val)

    assert bt_left.get_left_child_val() == left_left_val


def test_binary_tree_get_left_right():
    root_val = '1'
    left_val = '1.1'
    left_right_val = '1.1.2'
    bt = BinaryTree(root_val)
    bt_left = bt.insert_left_child(left_val)
    bt_left.insert_left_child(left_right_val)

    assert bt_left.get_left_child_val() == left_right_val


def test_binary_tree_preorder():
    A = BinaryTree('A')
    A_B = A.insert_left_child('B')
    A_C = A.insert_right_child('C')

    A_B_D = A_B.insert_left_child('D')
    A_B.insert_right_child('E')

    A_B_D.insert_left_child('H')
    A_B_D.insert_right_child('I')

    A_C.insert_left_child('F')
    A_C_G = A_C.insert_right_child('G')
    A_C_G.insert_left_child('J')

    assert A.preorder() == 'ABDHIECFGJ'


def test_binary_tree_postorder():
    A = BinaryTree('A')
    A_B = A.insert_left_child('B')
    A_C = A.insert_right_child('C')

    A_B_D = A_B.insert_left_child('D')
    A_B.insert_right_child('E')

    A_B_D.insert_left_child('H')
    A_B_D.insert_right_child('I')

    A_C.insert_left_child('F')
    A_C_G = A_C.insert_right_child('G')
    A_C_G.insert_left_child('J')

    assert A.postorder() == 'HIDEBFJGCA'


def test_binary_tree_inorder():
    A = BinaryTree('A')
    A_B = A.insert_left_child('B')
    A_C = A.insert_right_child('C')

    A_B_D = A_B.insert_left_child('D')
    A_B.insert_right_child('E')

    A_B_D.insert_left_child('H')
    A_B_D.insert_right_child('I')

    A_C.insert_left_child('F')
    A_C_G = A_C.insert_right_child('G')
    A_C_G.insert_left_child('J')

    assert A.inorder() == 'HDIBEAFCJG'

