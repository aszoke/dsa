from ..binary_tree_list_based import *


def test_binary_tree_list_based_insert_root():
    root_val = 1
    root = binary_tree(root_val)

    assert get_root_val(root) == root_val


def test_binary_tree_list_based_insert_left():
    root_val = 1
    left_val = 2

    root = binary_tree(root_val)
    insert_left(root, left_val)

    assert get_left_child(root)


def test_binary_tree_list_based_insert_right():
    root_val = 1
    right_val = 2

    root = binary_tree(root_val)
    insert_right(root, right_val)

    assert get_right_child(root)


def test_binary_tree_list_based_insert_right_and_right():
    root_val = 1
    right_val = 2
    right_val2 = 3

    root = binary_tree(root_val)
    left_node = insert_right(root, right_val)
    insert_right(left_node, right_val2)

    assert get_right_child(left_node)
