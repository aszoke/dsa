"""
Binary tree module
"""


class BinaryTree:
    """
    **BINARY TREE** a binary tree is a tree data structure in which each node has at most two children, which are
    referred to as the left child and the right child.

    **See also**
    :mod:`binary_tree_list_based.binary_tree`
    """

    pre_order = ''      # static variable for storing preorder value during recursion
    post_order = ''     # static variable for storing postorder value during recursion
    in_order = ''       # static variable for storing inorder value during recursion

    def __init__(self, root: str):
        self.key = root
        self.left_child = None
        self.right_child = None
        self.pre_order = ''

    def insert_left_child(self, new_node):
        if self.left_child is None:  # no child then add a node to the tree
            self.left_child = BinaryTree(new_node)
        else:  # push the existing child down one level in the tree
            tree = BinaryTree(new_node)
            tree.left_child = self.left_child
            self.left_child = tree
        return self.left_child

    def insert_right_child(self, new_node):
        if self.right_child is None:
            self.right_child = BinaryTree(new_node)
        else:
            tree = BinaryTree(new_node)
            tree.right_child = self.right_child
            self.right_child = tree
        return self.right_child

    def get_right_child(self):
        return self.right_child

    def get_left_child(self):
        return self.left_child

    def get_right_child_val(self):
        return self.right_child.key

    def get_left_child_val(self):
        return self.left_child.key

    def set_node_val(self, obj):
        self.key = obj

    def get_node_val(self):
        return self.key

    def preorder_helper(self):
        # We are collecting node values when we are entering to a node
        BinaryTree.pre_order = BinaryTree.pre_order + self.key
        if self.left_child:
            self.left_child.preorder_helper()
            pass
        if self.right_child:
            self.right_child.preorder_helper()
            pass

    def preorder(self) -> str:
        BinaryTree.pre_order = ''
        self.preorder_helper()
        return BinaryTree.pre_order

    def postorder_helper(self):
        if self.left_child:
            self.left_child.postorder_helper()
            pass
        if self.right_child:
            self.right_child.postorder_helper()
            pass
        # We are collecting node values when we are returning from a given node
        BinaryTree.post_order = BinaryTree.post_order + self.key

    def postorder(self) -> str:
        BinaryTree.post_order = ''
        self.postorder_helper()
        return BinaryTree.post_order

    def inorder_helper(self):
        if self.left_child:
            self.left_child.inorder_helper()
            pass
        # We are collecting node values when we are returning from the left node
        BinaryTree.in_order = BinaryTree.in_order + self.key
        if self.right_child:
            self.right_child.inorder_helper()
            pass

    def inorder(self) -> str:
        BinaryTree.in_order = ''
        self.inorder_helper()
        return BinaryTree.in_order
