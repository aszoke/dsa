"""
Binary search of closest module
"""


def binary_search_of_closest(a_olist: list, item: int) -> (int, int):
    """
    **BINARY SEARCH OF THE CLOSEST** is searching for the closest item in an ordered list.

    The algorithm usese the binary search algorithm that takes advantage of a collection of elements that is already
    sorted.

    :param a_olist:
        ordered list of items
    :param item:
        searchable list
    :return:
        closest list, difference of the searchable and the returned closest list

    **Complexity**
    A binary search of an ordered list is 𝑂(log 𝑛) in the worst case.
    """

    min_pos = 0
    max_pos = len(a_olist)-1
    while not (max_pos - min_pos) <= 1:
        pos = (max_pos + min_pos) // 2
        current_item = a_olist[pos]
        if current_item <= item:
            min_pos = pos
        elif current_item > item:
            max_pos = pos

    lower_delta = abs(a_olist[min_pos] - item)
    upper_delta = abs(a_olist[max_pos] - item)
    if lower_delta <= upper_delta:
        ret = a_olist[min_pos]
        delta = lower_delta
    else:
        ret = a_olist[max_pos]
        delta = upper_delta

    return ret, delta
