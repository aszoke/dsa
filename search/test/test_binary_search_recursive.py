import pytest

from search import binary_search_recursive as bsr
from . import data


@pytest.mark.parametrize("input_params, expected", data.ordered_list_case)
def test_binary_search_recursive(input_params, expected):
    assert bsr.binary_search_recursive(input_params[0], input_params[1]) == expected


def test_binary_recursive_search_huge_list(random_ordered_list):
    assert bsr.binary_search_recursive(random_ordered_list[0][0], random_ordered_list[0][1]) == random_ordered_list[1]
