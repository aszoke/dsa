import pytest
from search import sequential_search as ss
from . import data


@pytest.mark.parametrize("input_params, expected", data.list_case)
def test_sequential_search(input_params, expected):
    assert ss.sequential_search(input_params[0], input_params[1]) == expected


def test_sequential_search_huge_list(random_list):
    assert ss.sequential_search(random_list[0][0], random_list[0][1]) == random_list[1]
