import pytest

from ..binary_search_tree import BinarySearchTree


@pytest.fixture
def sample_tree():
    sample_tree = BinarySearchTree()
    sample_tree.put(5, "a")
    sample_tree.put(30, "b")
    sample_tree.put(2, "c")
    sample_tree.put(40, "d")
    sample_tree.put(25, "e")
    sample_tree.put(45, "f")

    return sample_tree


def test_binary_search_tree_empty():
    st = BinarySearchTree()
    assert st.is_empty()


def test_binary_search_tree_not_empy():
    st = BinarySearchTree()
    st.put(1, "a")
    assert not st.is_empty()


def test_binary_search_tree_lookup_found(sample_tree):
    tn = sample_tree.get(5)
    assert tn.value == "a"


def test_binary_search_tree_lookup_not_found(sample_tree):
    tn = sample_tree.get(6)
    assert tn is None


def test_binary_search_tree_lookup_contains_true(sample_tree):
    assert sample_tree.contains(5)


def test_binary_search_tree_lookup_contains_false(sample_tree):
    assert not sample_tree.contains(6)


def test_binary_search_tree_size(sample_tree):
    assert sample_tree.size() == 6


def test_binary_search_tree_delete(sample_tree):
    sample_tree.delete(45)

    assert sample_tree.size() == 5


def test_binary_search_tree_delete_all(sample_tree):
    sample_tree.delete(5)
    sample_tree.delete(30)
    sample_tree.delete(2)
    sample_tree.delete(40)
    sample_tree.delete(25)
    sample_tree.delete(45)

    assert sample_tree.size() == 0


def test_binary_search_tree_find_min(sample_tree):
    node = sample_tree.find_min()

    assert node.value == "c"


def test_binary_search_tree_find_max(sample_tree):
    node = sample_tree.find_max()

    assert node.value == "f"
