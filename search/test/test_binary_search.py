import pytest

from search import binary_search as bs
from . import data


@pytest.mark.parametrize("input_params, expected", data.ordered_list_case)
def test_binary_search(input_params, expected):
    assert bs.binary_search(input_params[0], input_params[1]) == expected


def test_binary_search_huge_list(random_ordered_list):
    assert bs.binary_search(random_ordered_list[0][0], random_ordered_list[0][1]) == random_ordered_list[1]
