"""
 Data sets (input and expected outputs) for tests
"""
list_case = [
    (([1, 2, 32, 8, 17, 19, 42, 13, 0], 3), False),
    (([1, 2, 32, 8, 17, 19, 42, 13, 0], 2), True)
]

ordered_list_case = [
    (([0, 1, 2, 8, 13, 17, 19, 32, 42], 3), False),
    (([0, 1, 2, 8, 13, 17, 19, 32, 42], 2), True)
]
