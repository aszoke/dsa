import pytest

from search import binary_search_of_closest as bsc


ordered_list_case_closest = [
    (([0, 1, 2, 8, 13, 17, 19, 32, 42], 3), (2, 1)),
    (([0, 1, 2, 8, 13, 17, 19, 32, 42], 15), (13, 2)),
    (([5, 7, 8, 11, 12, 13, 14, 17, 18, 19, 21, 22, 24, 25, 26, 35, 39, 41, 48, 52, 53, 56, 57, 60, 62, 63, 67, 72, 74,
     75, 76, 81, 82, 88, 91, 92], 38), (39, 1)),
    (([5, 7, 8, 11, 12, 13, 14, 17, 18, 19, 21, 22, 24, 25, 26, 35, 39, 41, 48, 52, 53, 56, 57, 60, 62, 63, 67, 72, 74,
     75, 76, 81, 82, 88, 91, 92], 39), (39, 0))
]


@pytest.mark.parametrize("input_params, expected", ordered_list_case_closest)
def test_binary_search_of_closest(input_params, expected):
    closest, delta = bsc.binary_search_of_closest(input_params[0], input_params[1])
    assert closest == expected[0] and delta == expected[1]
