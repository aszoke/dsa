"""
Binary search tree module
"""


class TreeNode:
    """
    Datatype for BinarySearchTree
    """
    def __init__(self, key=None, value=None, parent=None):
        self.key = key
        self.value = value
        self.parent = parent
        self.left = None
        self.right = None

    def has_left_child(self) -> bool:
        return self.left is not None

    def has_right_child(self) -> bool:
        return self.right is not None

    def has_both_children(self) -> bool:
        return self.has_left_child() and self.has_right_child()

    def is_leaf(self) -> bool:
        return not self.has_left_child() and not self.has_right_child()

    def is_root(self) -> bool:
        return self.parent is None

    def has_parent(self) -> bool:
        return self.parent is not None

    def is_left_child(self) -> bool:
        return self.parent.left == self

    def is_right_child(self) -> bool:
        return self.parent.right == self

    def find_min(self):
        if self is None:
            return None
        if self.has_left_child():
            return self.left.find_min()
        else:
            return self

    def find_max(self):
        if self is None:
            return None
        node = self
        while node.right is not None:
            node = node.right
        return node


class BinarySearchTree:
    """
    **BINARY SEARCH TREE**  (also called an ordered or sorted binary tree) is a rooted binary tree whose internal
    nodes each store a key greater than all the keys in the node's left subtree and less than those in its right
    subtree. A binary tree is a type of data structure for storing data such as numbers in an organized way. Binary
    search trees allow binary search for fast lookup, addition and removal of data items, and can be used to
    implement dynamic sets and lookup tables.

        The implementation uses two classes: the BinarySearchTree, and the BSTNode. The BinarySearchTree class has a
        reference to the BSTNode that is the root of the binary search tree.

        **BST is used for**:
         * parsing and evaluating expressions
         * map ADT
         * min heap

        **BST property**: A binary search tree relies on the property that keys that are less than the parent are
        found in the left subtree, and keys that are greater than the parent are found in the right subtree.
    """

    def __init__(self):
        self.root: TreeNode or None = None
        self.elements: int = 0

    def size(self) -> int:
        return self.elements

    def is_empty(self) -> bool:
        return self.root is None

    # Method to build our binary search tree
    # FIXME: implementation of insert is that duplicate keys are not handled properly. As our tree is implemented a
    #  duplicate key will create a new node
    #  with the same key value in the right subtree of the node having the original key. The result of this is
    #  that the node
    #  with the new key will never be found during a search. A better way to handle the insertion of
    #  a duplicate key is for the value associated with the new key to replace the old value.
    def put(self, key, value):
        if self.is_empty():     # check to see if the tree already has a root
            self.root = TreeNode(key, value)
            self.elements += 1
        else:
            self._put(self.root, key, value)    # if it has element calls the private _put

    # Recursive helper function to place item into the tree following the next algorithm:
    # 1) Starting at the root of the tree, search the binary tree comparing the new key to the key in
    # the current node. If the new key is less than the current node, search the left subtree. If
    # the new key is greater than the current node, search the right subtree.
    # 2) When there is no left (or right) child to search, we have found the position in the tree
    # where the new node should be placed as a new TreeNode object.
    def _put(self, root: TreeNode, key, value):
        if root.key == key:
            root.value = value
        elif key < root.key:
            if root.has_left_child():
                self._put(root.left, key, value)
            else:
                root.left = TreeNode(key, value, root)
                self.elements += 1
        else:
            if root.has_right_child():
                self._put(root.right, key, value)
            else:
                root.right = TreeNode(key, value, root)
                self.elements += 1

    # Method to retrieve item by key from our binary search tree
    def get(self, key) -> TreeNode or None:
        if self.is_empty():
            return None
        else:
            return self._get(self.root, key)    # if it has element calls the private _get

    # The search in this method uses the same logic for choosing the left or right child as the _put method.
    def _get(self, root: TreeNode, key) -> TreeNode or None:
        if root.key == key:
            return root
        elif key < root.key:
            if root.has_left_child():
                return self._get(root.left, key)
            else:
                return None
        else:
            if root.has_right_child():
                return self._get(root.right, key)
            else:
                return None

    def contains(self, key) -> bool or None:
        if self.is_empty():
            return None
        found: bool = False
        node: TreeNode = self.root
        while node is not None and not found:
            if node.key == key:
                found = True
            elif key < node.key:
                node = node.left
            else:
                node = node.right
        return found

    def delete(self, key):
        node_to_delete: TreeNode = self.get(key)
        if node_to_delete is None:
            return
        if node_to_delete.is_root():
            if node_to_delete.is_leaf():
                self.root = None
                self.elements -= 1
            elif node_to_delete.has_both_children():
                max_node: TreeNode = node_to_delete.left.find_max()
                tmp_key = max_node.key
                tmp_value = max_node.value
                self.delete(tmp_key)
                node_to_delete.key = tmp_key
                node_to_delete.value = tmp_value
            else:
                if node_to_delete.has_left_child():
                    self.root = node_to_delete.left
                else:
                    self.root = node_to_delete.right
                self.root.parent = None
                self.elements -= 1
        else:
            if node_to_delete.is_leaf():
                if node_to_delete.is_left_child():
                    node_to_delete.parent.left = None
                else:
                    node_to_delete.parent.right = None
                self.elements -= 1
            elif node_to_delete.has_both_children():
                max_node: TreeNode = node_to_delete.left.find_max()
                tmp_key = max_node.key
                tmp_value = max_node.value
                self.delete(tmp_key)
                node_to_delete.key = tmp_key
                node_to_delete.value = tmp_value
            elif node_to_delete.has_left_child():
                self.elements -= 1
                if node_to_delete.is_left_child():
                    node_to_delete.parent.left = node_to_delete.left
                else:
                    node_to_delete.parent.right = node_to_delete.left
                node_to_delete.left.parent = node_to_delete.parent
            else:
                self.elements -= 1
                if node_to_delete.is_left_child():
                    node_to_delete.parent.left = node_to_delete.right
                else:
                    node_to_delete.parent.right = node_to_delete.right
                node_to_delete.right.parent = node_to_delete.parent

    def find_min(self) -> TreeNode or None:
        if self.is_empty():
            return None
        node: TreeNode = self.root
        while node.left is not None:
            node = node.left
        return node

    def find_max(self) -> TreeNode or None:
        if self.is_empty():
            return None
        node: TreeNode = self.root
        while node.right is not None:
            node = node.right
        return node
