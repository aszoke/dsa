"""
Sequential search module
"""


def sequential_search(a_list: list, item: int) -> bool:
    """
    **SEQUENTIAL SEARCH** is searching list in a list using the sequential search method.

    Starting at the first element in the list, we simply move from item to item, following the underlying sequential
    ordering until we either find what we are looking for or run out of items.

    :param a_list:
        list of items
    :param item:
        searchable item
    :return:
        is found

    **Complexity**
    A sequential search is 𝑂(𝑛) for ordered and unordered lists.
    """

    found = False
    for an_item in a_list:
        if an_item == item:
            found = True
            break
    return found
