"""
Binary search recursive module
"""


def binary_search_recursive(a_olist: list, item: int, *args) -> bool:
    """
    **BINARY SEARCH RECURSIVE** is searching item in an ordered list using the binary search method in a recursive way.

    Binary search algorithm takes advantage of a collection of elements that is already sorted by ignoring half of the
    elements after just one comparison.

    :param a_olist:
        ordered list of items
    :param item:
        searchable list
    :return:
        is found

    **Complexity**
    A binary search of an ordered list is 𝑂(log 𝑛) in the worst case. Implemented in a recursive approach.

    **See also**
    :mod:`search.binary_search`
    """

    if args:
        low = args[0]
        high = args[1]
    else:
        low = 0
        high = len(a_olist)

    # Check base case
    if high >= low:

        mid = (high + low) // 2

        # Element is present at the middle of the array
        if a_olist[mid] == item:
            return True

        # If element is smaller than it can only be present in left subarray
        elif a_olist[mid] > item:
            return binary_search_recursive(a_olist, item, low, mid - 1)

        # The element can only be present in right subarray
        else:
            return binary_search_recursive(a_olist, item, mid + 1, high)

    else:
        # Element is not present in the array
        return False
