"""
Binary search iterative module
"""


def binary_search(a_olist: list, item: int) -> bool:
    """
    **BINARY SEARCH** is searching item in an ordered list using the binary search method in an iterative way.

    Binary search algorithm takes advantage of a collection of elements that is already sorted by ignoring half of the
    elements after just one comparison.

    Start by examining the middle item. If that item is the one we are searching for, we are done. If it is not the
    correct item, we can use the ordered nature of the list to eliminate half of the remaining items. If the item we are
     searching for is greater than the middle item, we know the item at the entire lower half of the list as well as
    the middle item can be eliminated from further consideration. The item, if it is in the list, must be in the upper
    half.

    :param a_olist:
        ordered list of items
    :param item:
        searchable item
    :return:
        is found

    **Complexity**
    A binary search of an ordered list is 𝑂(log 𝑛) in the worst case. Implemented in a iterative approach.

    **See also**
    :mod:`search.binary_search_recursive`
    """

    found = False
    min_pos = 0
    max_pos = len(a_olist) - 1
    while not found and max_pos != min_pos:
        pos = (max_pos + min_pos) // 2
        current_item = a_olist[pos]
        if current_item == item:
            found = True
        elif current_item < item:
            min_pos = pos + 1
        else:
            max_pos = pos - 1
    return found
